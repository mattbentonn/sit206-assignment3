//
//  ResultsViewController.swift
//  Assignment2-SIT206
//
//  Created by matt on 25/5/18.
//  Copyright © 2018 matt. All rights reserved.
//

import Foundation
import UIKit

class ResultsViewController : UIViewController, UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.PlayerResults.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultCell", for: indexPath);
        
        let name = self.PlayerResults[indexPath.row].player;
        let result = self.PlayerResults[indexPath.row].score;
        let score = String("\(abs(self.Target! - Int(result)!))");
        cell.textLabel?.text = "Player: " + name + "\t | Score: " + score + " away";
        
        return cell;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    var MPCService = MPCManager.GlobalMPCManager;
    var PlayerResults : [(player : String ,score : String)] = [];
    var Target : Int?;
    
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var resultsTableView: UITableView!
    override func viewDidLoad() {
        MPCService.delegate = self;
        resultsTableView.delegate = self;
        resultsTableView.dataSource = self;
        targetLabel.text = String("Your target was: \(Target!)");
        DispatchQueue.main.async{
            self.resultsTableView.reloadData();
        }
        
    }
    
    @IBAction func PlayAgain(_ sender: UIButton) {
        MPCService.session.disconnect();
        MPCService.broadcast();
        
        weak var nextVC = storyboard?.instantiateViewController(withIdentifier: "LobbyVC") as? LobbyViewController;
        nextVC?.DisplayName = MPCService.myPeerID.displayName;
        self.present(nextVC!, animated: true, completion: nil);
        
    }
    
}

extension ResultsViewController : MPCManagerDelegate {
    func connectedDevicesChanged(manager: MPCManager, connectedDevices: [String]) {
        //nothing here
    }
    
    func dataReceived(manager: MPCManager, fromPeer: String, myData: Data) {
        //decode the data recieved and depending on the Key, do something with it.
        do{
            let decoded = try JSONSerialization.jsonObject(with: myData, options: [])
            if let dictFromJSON = decoded as? [String : String]{
                print(fromPeer + ": ");
                for(key, value) in dictFromJSON{
                    switch(key){
                    case "results":
                        self.PlayerResults.append((fromPeer, value));
                        let tempArr = self.PlayerResults.sorted(by: {abs(self.Target! - Int($0.score)!) < abs(self.Target! - Int($1.score)!)});
                        self.PlayerResults = tempArr;
                    default:
                        print("default");
                    }
                }
            }
            DispatchQueue.main.async{
                self.resultsTableView.reloadData();
            }
        }catch{
            print(error.localizedDescription);
        }
    }
    
    
}
