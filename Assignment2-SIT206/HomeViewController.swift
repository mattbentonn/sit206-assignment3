//
//  ViewController.swift
//  Assignment2-SIT206
//
//  Created by matt on 6/5/18.
//  Copyright © 2018 matt. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var DisplayName: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func Search(_ sender: Any) {
        MPCManager.myDisplayName = DisplayName.text!;
        weak var MPCService = MPCManager.GlobalMPCManager;
        
        MPCService?.broadcast();
        weak var nextVC = storyboard?.instantiateViewController(withIdentifier: "LobbyVC") as? LobbyViewController;
        nextVC?.DisplayName = DisplayName.text!;
        self.present(nextVC!, animated: true, completion: nil);
    }
    
}

