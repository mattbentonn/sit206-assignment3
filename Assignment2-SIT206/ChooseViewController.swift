//
//  ChooseViewController.swift
//  Assignment2-SIT206
//
//  Created by matt on 22/5/18.
//  Copyright © 2018 matt. All rights reserved.
//

import Foundation
import UIKit

class ChooseViewController : UIViewController {
    
    weak var MPCService = MPCManager.GlobalMPCManager;
    let game : Game = Game();
    
    @IBAction func ChooseNumber(_ sender: UIButton)
    {
        var numbersDict : [String : [Int]] = [:];
        game.GenerateLargeNumbers(amount: sender.tag);
        game.GenerateSmallNumbers(amount: 6 - sender.tag);
        game.GenerateTargetNumber();

        numbersDict["largeNumbers"] = game.LrgNumbers;
        numbersDict["smallNumbers"] = game.SmlNumbers;
        numbersDict["target"] = [game.TargetNumber] as? [Int];
        
        MPCService?.send(data:numbersDict)
        
        weak var nextVC = storyboard?.instantiateViewController(withIdentifier: "GameVC") as? GameViewController;
        nextVC?.game = game;
       // nextVC?.targetLabel.text = String("\(game.TargetNumber ?? 0)");
        self.present(nextVC!, animated: true, completion: nil);
    }
    
}
