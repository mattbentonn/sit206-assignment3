//
//  LobbyViewController.swift
//  Assignment2-SIT206
//
//  Created by matt on 6/5/18.
//  Copyright © 2018 matt. All rights reserved.
//

import UIKit


class LobbyViewController: UITableViewController{
    
    var DisplayName = "Player";
    var PlayerNamesDict : [String : Int] = [:];
    var PlayerNamesAndID : [String : Int] = [:];
    var StartGameDict : [String : Int] = [:];
    weak var MPCService = MPCManager.GlobalMPCManager;
    
    deinit {
        print("LobbyViewController has been deallocated.");
    }
    
    @IBAction func BackToHome(_ sender: Any) {
        MPCService?.stopBroadcast();
        weak var nextVC = storyboard?.instantiateViewController(withIdentifier: "HomeVC") as? HomeViewController;
        self.present(nextVC!, animated: true, completion: nil);
    }
    
    @IBAction func StartGame(_ sender: Any) {
        //choose a random number
        let n = Int(arc4random_uniform(1000));
        //send it to everyone else
        var str = "\(n)";
        
        MPCService?.send(data: ["start": str]);
        
        //make sure you add your own number to the list
        RecievedStartGameMessage(fromPeer: DisplayName, peerNumber: n);
    }
    
    func RecievedStartGameMessage(fromPeer : String, peerNumber : Int){
        //add random number to the list
        StartGameDict[fromPeer] = peerNumber;
        
        //once everyone that is in the PlayerNamesAndID list has added their random number to the list
        if(StartGameDict.count == PlayerNamesAndID.count){
            var number = 0;
            //add all the numbers together
            for value in StartGameDict.values{
                number += value;
            }
            
            //find the remainder when dividing that number by the number of players
            //if this player's ID is the same as that remainder send them to the Choose Screen
            //else send them to the game screen.
            MPCService?.stopBroadcast();
            DispatchQueue.main.async{
                if(number % self.PlayerNamesAndID.count == self.PlayerNamesAndID[self.DisplayName]){
                    weak var nextVC = self.storyboard?.instantiateViewController(withIdentifier: "ChooseVC") as? ChooseViewController;
                    self.present(nextVC!, animated: true, completion: nil);
                }else{
                    weak var nextVC = self.storyboard?.instantiateViewController(withIdentifier: "GameVC") as? GameViewController;
                    self.present(nextVC!, animated: true, completion: nil);
                }
            }
        }
    }
    
    func SortPlayers(){
        
        //sort players and then give them an ID value based on their order.
        let dict = self.PlayerNamesDict.sorted(by: {$0.value < $1.value});
        var count = 0;
        for (key, value) in dict{
            self.PlayerNamesAndID[key] = count;
            count += 1;
        }
        
        //reload the table once thats done.
        OperationQueue.main.addOperation {
            self.tableView.reloadData();
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        PlayerNamesDict[DisplayName] = MPCService!.RandomNumber;
        self.SortPlayers();
        MPCService?.delegate = self;
      
        tableView.beginUpdates();
        tableView.insertRows(at:[IndexPath(row: Array(PlayerNamesAndID.keys).count-1, section:Array(PlayerNamesAndID.keys).count)], with: .automatic)
        tableView.endUpdates();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PlayerNamesDict.count;
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "lobbyCell", for: indexPath);
        
        let playerName = Array(PlayerNamesDict.keys)[indexPath.row];
        
        cell.textLabel?.text = playerName;
        
        return cell;
    }
}

extension LobbyViewController : MPCManagerDelegate {
  
    func connectedDevicesChanged(manager: MPCManager, connectedDevices: [String]) {
        print("data received");
        var str = "";
        if let v = MPCService?.RandomNumber{
            str = "\(v)";
        }
        MPCService?.send(data: ["assignID": str]);
     
    }
    
    func dataReceived(manager: MPCManager, fromPeer: String , myData: Data) {
        
        //decode the data recieved and depending on the Key, do something with it.
        do{
        let decoded = try JSONSerialization.jsonObject(with: myData, options: [])
        if let dictFromJSON = decoded as? [String : String]{
            print(fromPeer + ": ");
            for(key, value) in dictFromJSON{
                switch(key){
                case "assignID":
                    self.PlayerNamesDict[fromPeer] = Int(value);
                    self.SortPlayers();
                case "start":
                    print(value);
                    self.RecievedStartGameMessage(fromPeer: fromPeer, peerNumber: Int(value)!);
                case "seedNumber":
                    print(value);
                    
                default:
                    print("default");
                }
            }
        }
        }catch{
            print(error.localizedDescription);
        }
        
    }
    
   
    
}


