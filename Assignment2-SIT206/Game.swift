//
//  Game.swift
//  Assignment2-SIT206
//
//  Created by matt on 6/5/18.
//  Copyright © 2018 matt. All rights reserved.
//

import Foundation

enum GAME_ACTIONS{
    case ADD
    case SUBTRACT
    case MULTIPLY
    case DIVIDE
    case EQUAL
    case NUMBER
    case STOREDNUMBER
}

class Game {
    
    var LrgNumberSelection = [25,50,75,100];
    
    
    var SmlNumbers : [Int]?;
    var LrgNumbers : [Int]?;
    var AllNumbers : [Int] = [];
    var TargetNumber: Int?;
    var StoredNumbers: [Int] = [];
    var Equations : [String] = [];
    var currentEquation: String = "";
    var UsedNumbers : [Int] = [];
    var UsedStoredNumbers : [Int] = [];
    var isNumberTurn : Bool = true;
    var currentActionsDict : [(action: GAME_ACTIONS, str: String)] = [];
    var ActionsDict : [(action: GAME_ACTIONS, str: String)] = [];
    var DeletedActions : [(action: GAME_ACTIONS, str: String)] = [];
    
    
    var ConnectedPlayers: [Player]?;
    var MainPlayer : Player = Player();
    
    func GenerateLargeNumbers(amount: Int){
        var times = amount;
        var nmbrsToReturn = [Int]();
        if times > 4{ times = 4 };
        if(times != 0){
            for _ in 1...times{
                let randomIndex = Int(arc4random_uniform(UInt32(LrgNumberSelection.count)))
                nmbrsToReturn.append(LrgNumberSelection[randomIndex]);
                LrgNumberSelection.remove(at:randomIndex);
            }
        }
        
        LrgNumbers = nmbrsToReturn;
        AllNumbers.append(contentsOf: nmbrsToReturn);
    }
    
    func GenerateSmallNumbers(amount: Int){
        var times = amount;
        var nmbrsToReturn = [Int]();
        if times > 6{ times = 6 };
        for _ in 1...times{
            nmbrsToReturn.append(Int(arc4random_uniform(10))+1);
        }
        
        SmlNumbers = nmbrsToReturn;
        AllNumbers.append(contentsOf: nmbrsToReturn);
    }
    
    func GenerateTargetNumber(){
        var nmbrToReturn : Int;
        nmbrToReturn = 100 + Int(arc4random_uniform(900));
        TargetNumber = nmbrToReturn;
    }
    
    
    
    
}
