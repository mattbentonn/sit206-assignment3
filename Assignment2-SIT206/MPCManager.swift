//
//  MultipeerController.swift
//  Assignment2-SIT206
//
//  Created by matt on 8/5/18.
//  Copyright © 2018 matt. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class MPCManager : NSObject{
    static var myDisplayName = "Player";
    static let GlobalMPCManager = MPCManager(myDisplayName: myDisplayName); //singleton
    let RandomNumber : Int = Int(arc4random_uniform(100000));
    
    let ServiceType = "ftn-game";
    
    var myPeerID = MCPeerID(displayName: UIDevice.current.name);
    
    let serviceAdvertiser : MCNearbyServiceAdvertiser;
    let serviceBrowser : MCNearbyServiceBrowser;
    
    var delegate : MPCManagerDelegate!
    
    private init(myDisplayName: String) {
        self.myPeerID = MCPeerID( displayName: myDisplayName);
        self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerID, discoveryInfo: nil, serviceType: ServiceType);
        self.serviceBrowser = MCNearbyServiceBrowser(peer: myPeerID, serviceType: ServiceType);
        
        super.init();
        
        self.serviceAdvertiser.delegate = self;
        self.serviceBrowser.delegate = self;
        
    }
    
    deinit {
        self.stopBroadcast();
        self.session.disconnect();
    }
    
    lazy var session : MCSession = {
        let session = MCSession(peer: self.myPeerID, securityIdentity: nil, encryptionPreference: .required);
        session.delegate = self;
        return session;
    }()
    
    func broadcast(){
        self.serviceAdvertiser.startAdvertisingPeer();
        self.serviceBrowser.startBrowsingForPeers();
    }
    
    func stopBroadcast(){
        self.serviceAdvertiser.stopAdvertisingPeer();
        self.serviceBrowser.stopBrowsingForPeers();
        
    }
    
    func send(data: [String : String]){
        NSLog("%@", "sendData: \(data) to \(session.connectedPeers.count) peers.");
        if (session.connectedPeers.count > 0){
            do {
                let encodedData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted);
                try self.session.send(encodedData, toPeers: session.connectedPeers, with:. reliable);
            }
            catch let error {
                NSLog("%@", "Error for sending: \(error)");
            }
        }
    }
    
    func send(data: [String : [Int]]){
        NSLog("%@", "sendData: \(data) to \(session.connectedPeers.count) peers.");
        if (session.connectedPeers.count > 0){
            do {
                let encodedData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted);
                try self.session.send(encodedData, toPeers: session.connectedPeers, with:. reliable);
            }
            catch let error {
                NSLog("%@", "Error for sending: \(error)");
            }
        }
    }
}

extension MPCManager : MCNearbyServiceAdvertiserDelegate {
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        NSLog("%@", "didNotStartAdvertisingPeer: \(error)");
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void){
         NSLog("%@", "didReceiveInvitationFromPeer: \(peerID)");
        invitationHandler(true, self.session);
    }
}

extension MPCManager : MCNearbyServiceBrowserDelegate {
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        NSLog("%@", "didNotStartBrowsingForPeers: \(error)");
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        NSLog("%@", "foundPeer: \(peerID)");
        NSLog("%@", "invitePeer: \(peerID)");
        browser.invitePeer(peerID, to: self.session, withContext: nil, timeout: 10);
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        NSLog("%@", "lostPeer: \(peerID)");
    }
    
}

extension MPCManager : MCSessionDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        NSLog("%@", "peer \(peerID) didChangeState: \(state.rawValue)");
        self.delegate?.connectedDevicesChanged(manager: self, connectedDevices: session.connectedPeers.map{$0.displayName})
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        NSLog("%@", "didReceiveData: \(data)");
        //let str = String(data: data, encoding: .utf8)!;
        //let json = String(data: encodedData, encoding: String.Encoding.utf8);
        //print(json);
        self.delegate?.dataReceived(manager: self, fromPeer: peerID.displayName, myData: data);
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        NSLog("%@", "didReceiveStream");
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        NSLog("%@", "didStartReceivingResourceWithName");
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        NSLog("%@", "didFinishReceivingResourceWithName");
    }
    
}

protocol MPCManagerDelegate {
    
    func connectedDevicesChanged(manager : MPCManager, connectedDevices: [String]);
    func dataReceived(manager: MPCManager, fromPeer: String, myData: Data)
}
