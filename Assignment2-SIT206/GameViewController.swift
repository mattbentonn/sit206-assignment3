//
//  GameViewController.swift
//  Assignment2-SIT206
//
//  Created by matt on 21/5/18.
//  Copyright © 2018 matt. All rights reserved.
//

import Foundation
import UIKit

class GameViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var equationsTableView: UITableView!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet var numberButtons: [UIButton]!
    @IBOutlet var storedNumberButtons: [UIButton]!
    @IBOutlet var actionButtons: [UIButton]!
    @IBOutlet weak var timerLabel: UILabel!
    
    
    var game : Game = Game();
    weak var MPCService = MPCManager.GlobalMPCManager;
    var timer = Timer();
    var seconds = 30;
    
    var PlayerResults : [(player : String ,score : String)] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad();
        MPCService?.delegate = self;
        equationsTableView.delegate = self;
        equationsTableView.dataSource = self;
        game.Equations.append("");
        
        for btn in storedNumberButtons{
            btn.isEnabled = false;
            btn.setTitle("---", for: .disabled);
        }
        
        for btn in actionButtons{
            btn.isEnabled = false;
        }
        
        if let target = game.TargetNumber{
            targetLabel.text = String("\(target)");
            runTimer();
        }
        
        
        if game.AllNumbers != [] {
            var count = 0;
            for btn in numberButtons{
                btn.setTitle(String("\(game.AllNumbers[count])"), for: .normal)
                count += 1;
            }
        }
    }
    
    func runTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(GameViewController.updateTimer), userInfo: nil ,repeats: true)
    }
    @objc func updateTimer(){
        seconds -= 1;
        timerLabel.text = "\(seconds)";
        if(timer.isValid){
            if seconds <= 0 {EndGame();}
        }
    }
    
    func EndGame(){
        timer.invalidate();
        weak var nextVC = self.storyboard?.instantiateViewController(withIdentifier: "ResultsVC") as? ResultsViewController;
        nextVC?.Target = self.game.TargetNumber;
        let myScore = String("\(self.game.StoredNumbers.last!)");
        self.PlayerResults.append(((MPCService?.myPeerID.displayName)!, myScore));
        
        let tempArr = self.PlayerResults.sorted(by: {abs(self.game.TargetNumber! - Int($0.score)!) < abs(self.game.TargetNumber! - Int($1.score)!)});
        self.PlayerResults = tempArr;
        
        for res in PlayerResults{
            nextVC?.PlayerResults.append(res);
        }
        
        MPCService?.send(data:["results" : myScore]);
        self.present(nextVC!, animated: true, completion: nil);
        //send the last current Stored number and player name
        //create Results VC and present it.
        
    }
    @IBAction func TouchAction(_ sender: UIButton) {
        switch(sender.titleLabel?.text){
        case "+":
            game.currentActionsDict.append((GAME_ACTIONS.ADD,"+"));
            game.currentEquation += " + ";
            game.Equations[game.Equations.count - 1] = game.currentEquation;
        case "-":
            game.currentActionsDict.append((GAME_ACTIONS.SUBTRACT,"-"));
            game.currentEquation += " - ";
            game.Equations[game.Equations.count - 1] = game.currentEquation;
        case "x":
            game.currentActionsDict.append((GAME_ACTIONS.MULTIPLY,"*"));
            game.currentEquation += " x ";
            game.Equations[game.Equations.count - 1] = game.currentEquation;
        case "/":
            game.currentActionsDict.append((GAME_ACTIONS.DIVIDE,"/"));
            game.currentEquation += " / ";
            game.Equations[game.Equations.count - 1] = game.currentEquation;
        case "=":
           
            game.currentEquation += " = ";
           
            CalculateEquation();
            
        default:
            print("pressed something not tracked")
        }
        
        ToggleButtons(nmbrsActive: true)
        self.equationsTableView.reloadData();
        ScrollToBottom();
    }
    
    func ScrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.game.Equations.count-1, section:0)
            self.equationsTableView.scrollToRow(at: indexPath, at:.bottom, animated:true);
        }
    }
    
    func CalculateEquation(){
        var equationString = "";
        for action in game.currentActionsDict{
            equationString += action.str;
        }
        
        let expn = NSExpression(format: equationString);
        if let result = expn.expressionValue(with: nil, context: nil){
            print(game.currentEquation);
            print(result);
            game.currentActionsDict.append((GAME_ACTIONS.EQUAL,String("\(expn.expressionValue(with: nil, context: nil) ?? "Can't Calculate")")));
            game.StoredNumbers.append(result as! Int);
            var counter = game.StoredNumbers.count - 1;
            for btn in storedNumberButtons{
                if(counter >= 0){
                    btn.isEnabled = true;
                    btn.setTitle(String("\(game.StoredNumbers[counter])"), for: .normal);
                    counter -= 1;
                }else{
                    btn.isEnabled = false;
                    btn.setTitle("", for: .normal);
                }
            }
            game.currentEquation += String("\(result)");
            game.Equations[game.Equations.count - 1] = game.currentEquation;
            game.Equations.append("");
            game.ActionsDict += game.currentActionsDict;
            game.currentEquation = "";
            game.currentActionsDict = [];
        }
    }
    
    @IBAction func TouchNumber(_ sender: UIButton) {
        game.currentActionsDict.append((GAME_ACTIONS.NUMBER,(sender.titleLabel?.text)!));
        game.UsedNumbers.append(Int((sender.titleLabel?.text)!)!);
        game.currentEquation += (sender.titleLabel?.text)!;
        game.Equations[game.Equations.count - 1] = game.currentEquation;
        sender.isEnabled = false;
        self.equationsTableView.reloadData();
        ToggleButtons(nmbrsActive: false)
    }
    
    @IBAction func TouchStoredNumber(_ sender: UIButton) {
        
        game.currentActionsDict.append((GAME_ACTIONS.STOREDNUMBER,(sender.titleLabel?.text)!));
        game.currentEquation += (sender.titleLabel?.text)!;
        game.Equations[game.Equations.count - 1] = game.currentEquation;
        sender.isEnabled = false;
        self.equationsTableView.reloadData();
        let str = sender.titleLabel?.text;
        let index = game.StoredNumbers.index(of: Int(str!)!);
        game.UsedStoredNumbers.append(game.StoredNumbers[index!]);
        game.StoredNumbers.remove(at: index!);
        ToggleButtons(nmbrsActive: false)
        
    }
    
    @IBAction func DeleteEquation(_ sender: UIButton) {
        switch(sender.titleLabel?.text){
        case "Back":
            if game.currentActionsDict.count == 0 {
                MoveActionsToCurrent();
                CreateEquationFromCurrentActions();
                game.Equations[game.Equations.count - 1] = game.currentEquation;
                
            }else{
                game.DeletedActions.append(game.currentActionsDict.last!);
                game.currentActionsDict.removeLast();
                CreateEquationFromCurrentActions();
                game.Equations[game.Equations.count - 1] = game.currentEquation;
            }
        case "Delete":
            if game.currentActionsDict.count == 0 {
                MoveActionsToCurrent();
                var tmpArr = game.currentActionsDict;
                tmpArr.reverse();
                for act in tmpArr{
                    game.DeletedActions.append(act);
                }
                game.currentActionsDict = []
                CreateEquationFromCurrentActions();
                game.Equations[game.Equations.count - 1] = game.currentEquation;
                
            }else{
                game.currentActionsDict = []
                CreateEquationFromCurrentActions();
                game.Equations[game.Equations.count - 1] = game.currentEquation;
            }
        case "Clear":
            game.currentEquation = "";
            game.currentActionsDict = [];
            game.Equations = [""];
            game.ActionsDict = [];
            
        default:
            print("something went wrong!");
        }
        EnabledBtnsOfDeletedActions();
        self.equationsTableView.reloadData();
        
    }
    
    //slicing ActionsDict and moving the last bit to the current actions. used when deleting.
    func MoveActionsToCurrent(){
        //tempArr for manipulating ActionsDict
        var tempArr = game.ActionsDict;
        //set the index at the last element by default
        var index = game.ActionsDict.count - 1 ;
        if(tempArr.count > 0){
            //remove the last element, which is an EQUAL action. <-- important
            tempArr.remove(at: tempArr.count - 1);
            //reverse the array/
            tempArr.reverse();
            //find the index of the first EQUAL action
            // - if I didnt remove the last equal before reversing this would always return the first element.
            // - also, this is why index is set to the very end of the array at the start by default, incase no EQUAL's exist.
            if let i = tempArr.index(where: {(item) -> Bool in
                item.action == GAME_ACTIONS.EQUAL
            }){
                index = i;
            }
            //find the index of the EQUAL action in the non reversed array.
            // - technically, because I removed 1 element from the temp array,
            // - this should be  - (index + 1), but i want the index of the element straight after the EQUAL action,
            // - so im putting both the steps to get that index in one line.
            let oppositeIndex = game.ActionsDict.count - 1 - (index);
            //split the array using swift's ArraySplice syntax.
            game.currentActionsDict = Array(game.ActionsDict[oppositeIndex...]);
            if(game.currentActionsDict.count > 0){
                //turns out I want to remove the last EQUAL action anyway
                game.DeletedActions.append(game.currentActionsDict.last!);
                game.currentActionsDict.removeLast();
            }
            //make the ActionsDict equal to the actions before the last EQUAL action - tongue twister right there
            game.ActionsDict = Array(game.ActionsDict[..<oppositeIndex]);
            // and then remove the current last equation - this is what the tableview is linked to.
            game.Equations.removeLast();
        }
    }
    func ToggleButtons(nmbrsActive : Bool){
        if nmbrsActive {
            for btn in numberButtons{
                if game.UsedNumbers.contains(Int((btn.titleLabel?.text)!)!){
                    btn.isEnabled = false;
                }else{
                    btn.isEnabled = true;
                }
            }
            for btn in actionButtons{
                btn.isEnabled = false;
            }
        }else{
            for btn in actionButtons{
                btn.isEnabled = true;
            }
            for btn in numberButtons{
                btn.isEnabled = false;
            }
        }
    }
    func EnabledBtnsOfDeletedActions(){
        for act in game.DeletedActions{
            switch(act.action){
            case GAME_ACTIONS.NUMBER:
                for btn in numberButtons{
                    if btn.titleLabel?.text == act.str && !btn.isEnabled{
                        btn.isEnabled = true;
                        let index = game.UsedNumbers.index(where:  {(item) -> Bool in
                            item == Int(act.str);
                        });
                        game.UsedNumbers.remove(at: index!)
                        break;
                    }
                }
            case GAME_ACTIONS.STOREDNUMBER:
                let index = game.UsedStoredNumbers.index(of: Int(act.str)!);
                game.StoredNumbers.append(game.UsedStoredNumbers[index!]);
                game.UsedStoredNumbers.remove(at: index!);
                var counter = game.StoredNumbers.count - 1;
                for btn in storedNumberButtons{
                    if(counter >= 0){
                        btn.isEnabled = true;
                        btn.setTitle(String("\(game.StoredNumbers[counter])"), for: .normal);
                        counter -= 1;
                    }else{
                        btn.isEnabled = false;
                        btn.setTitle("", for: .normal);
                    }
                }
            case GAME_ACTIONS.EQUAL:
                let index = game.StoredNumbers.index(of: Int(act.str)!);
                game.StoredNumbers.remove(at: index!);
                var counter = game.StoredNumbers.count - 1;
                for btn in storedNumberButtons{
                    if(counter >= 0){
                        btn.isEnabled = true;
                        btn.setTitle(String("\(game.StoredNumbers[counter])"), for: .normal);
                        counter -= 1;
                    }else{
                        btn.isEnabled = false;
                        btn.setTitle("", for: .normal);
                    }
                }
            default:
                print("deleted an action:");
                
            }
        }
        
        if(game.DeletedActions.last?.action == GAME_ACTIONS.NUMBER ||
        game.DeletedActions.last?.action == GAME_ACTIONS.STOREDNUMBER || game.DeletedActions.count == 0)
        {
            ToggleButtons(nmbrsActive: true)
        }else{
            ToggleButtons(nmbrsActive: false)
        }
        game.DeletedActions = [];
    }
    
    func CreateEquationFromCurrentActions(){
        game.currentEquation = "";
        for action in game.currentActionsDict {
            switch(action.action){
            case GAME_ACTIONS.NUMBER:
                game.currentEquation += action.str;
            case GAME_ACTIONS.STOREDNUMBER:
                game.currentEquation += action.str;
            case GAME_ACTIONS.ADD:
                game.currentEquation += " + "
            case GAME_ACTIONS.SUBTRACT:
                game.currentEquation += " - "
            case GAME_ACTIONS.MULTIPLY:
                game.currentEquation += " x "
            case GAME_ACTIONS.DIVIDE:
                game.currentEquation += " / "
            case GAME_ACTIONS.EQUAL:
                game.currentEquation += "Should never be this"
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.game.Equations.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "equationCell", for: indexPath);
        
        let equation = game.Equations[indexPath.row];
        
        cell.textLabel?.text = equation;
        
        return cell;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
}

extension GameViewController : MPCManagerDelegate {
    
    func connectedDevicesChanged(manager: MPCManager, connectedDevices: [String]) {
        var str = "";
        if let v = MPCService?.RandomNumber{
            str = "\(v)";
        }
        MPCService?.send(data: ["assignID": str]);
        
    }
    
    func dataReceived(manager: MPCManager, fromPeer: String , myData: Data) {
        
        //decode the data recieved and depending on the Key, do something with it.
        do{
            let decoded = try JSONSerialization.jsonObject(with: myData, options: [])
            if let dictFromJSON = decoded as? [String : [Int]]{
                print(fromPeer + ": ");
                for(key, value) in dictFromJSON{
                    switch(key){
                    case "largeNumbers":
                        if(self.game.LrgNumbers == nil){
                            for nmb in value{
                                self.game.AllNumbers.append(nmb);
                            }
                        }
                        self.game.LrgNumbers = value
                        print(self.game.LrgNumbers!);
                        
                    case "smallNumbers":
                        if(self.game.SmlNumbers == nil){
                            for nmb in value{
                                self.game.AllNumbers.append(nmb);
                            }
                        }
                        self.game.SmlNumbers = value
                        print(self.game.SmlNumbers!);
                    case "target":
                        self.game.TargetNumber = value[0]
                        DispatchQueue.main.async {
                            self.targetLabel.text = String("\(self.game.TargetNumber ?? 0)");
                        }
                        print(self.game.TargetNumber!);
                    case "results":
                        self.PlayerResults.append((fromPeer, String("\(value)")));
                        
                    default:
                        print(key, value);
                        print("default");
                    }

                }
                if self.game.LrgNumbers?.count != 0 && self.game.SmlNumbers?.count != 0 && self.game.TargetNumber != nil{
                    if self.game.AllNumbers != [] {
                        var count = 0;
                        DispatchQueue.main.async{
                            for btn in self.numberButtons{
                                btn.setTitle(String("\(self.game.AllNumbers[count])"), for: .normal)
                                count += 1;
                            }
                            self.runTimer();
                        }
                    }
                }
            }
        }catch{
            print(error.localizedDescription);
        }
        
    }
    
    
    
}
